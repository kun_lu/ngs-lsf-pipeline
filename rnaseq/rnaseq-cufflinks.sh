#!/bin/bash

#
# Pipeline that submits jobs to a LSF cluster
# @author: sdentro
# @date: March 14, 2014
#
# Example run:
#	/rnaseq-cufflinks.sh $PWD/test_data/cases.fofn $PWD/test_data/controls.fofn $PWD/output
#
CASES=$1
CONTROLS=$2
OUTDIR=$3

source config

jobstowaitfor=''
list_of_cases=''
list_of_contols=''

mkdir ${OUTDIR}/01.samples

## Create a separate job for each input file that creates the counts
while read infile; do
	samplename=`basename ${infile}`
	mkdir ${OUTDIR}/01.samples/${samplename}
	cmd="$(bsubjob -J ${samplename}_cufflinks -M 1000 -q ${QUEUE} -c "${CUFFLINKS} -g ${GENESET} -p ${THREADS} -o ${OUTDIR}/01.samples/${samplename} ${infile}")"
	message=$(eval $cmd)
	jobid=`echo $message | cut -d "<" -f 2 | cut -d ">" -f 1`
	jobstowaitfor=$jobstowaitfor" && done($jobid)"
	list_of_cases=${list_of_cases}",$infile"
done < ${CASES}
list_of_cases=`echo $list_of_cases | sed 's/,//'`

while read infile; do
        samplename=`basename ${infile}`
	mkdir ${OUTDIR}/01.samples/${samplename}
        cmd="$(bsubjob -J ${samplename}_cufflinks -M 1000 -q ${QUEUE} -c "${CUFFLINKS} -g ${GENESET} -p ${THREADS} -o ${OUTDIR}/01.samples/${samplename} ${infile}")"
        message=$(eval $cmd)
        jobid=`echo $message | cut -d "<" -f 2 | cut -d ">" -f 1`
        jobstowaitfor=$jobstowaitfor" && done($jobid)"
	list_of_controls=${list_of_controls}",$infile"
done < ${CONTROLS}
list_of_controls=`echo $list_of_controls | sed 's/,//'`

## Remove first and-and
jobstowaitfor=`echo $jobstowaitfor | sed 's/\&\&//'`

## Submit cuffmerge job
mkdir ${OUTDIR}/02.assemble
cmd="$(bsubjob -J assemble -M 100 -q ${QUEUE} -w "\"${jobstowaitfor}\"" -c "ls ${OUTDIR}/01.samples/*/* | grep transcripts.gtf > ${OUTDIR}/02.assemble/assemblies.txt")"
echo $cmd
message=$(eval $cmd)
jobid=`echo $message | cut -d "<" -f 2 | cut -d ">" -f 1`

mkdir ${OUTDIR}/03.cuffmerge
cmd="$(bsubjob -J cuffmerge -M 100 -q ${QUEUE} -w "done\(${jobid}\)" -c "$CUFFMERGE -g ${GENESET} -s ${REF} -o ${OUTDIR}/03.cuffmerge ${OUTDIR}/02.assemble/assemblies.txt")"
echo $cmd
message=$(eval $cmd)
jobid=`echo $message | cut -d "<" -f 2 | cut -d ">" -f 1`

#cmd="$(bsubjob -J cuffcompare -M 1000 -q ${QUEUE} -w "done\(${jobid}\)" -c "${CUFFCOMPARE} -s ${REF} -r ${GENESET} ${OUTDIR}/merged.gtf")"
#message=$(eval $cmd)
#jobid=`echo $message | cut -d "<" -f 2 | cut -d ">" -f 1`

## Submit cuffdiff job
mkdir ${OUTDIR}/04.cuffdiff
cmd="$(bsubjob -J cuffdiff -w "done\(${jobid}\)" -M 1000 -q ${QUEUE} -c "${CUFFDIFF} -p ${THREADS} -o ${OUTDIR}/04.cuffdiff ${OUTDIR}/03.cuffmerge/merged.gtf ${list_of_cases} ${list_of_controls}")"
message=$(eval $cmd)
