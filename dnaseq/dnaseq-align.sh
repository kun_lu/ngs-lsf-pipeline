#!/bin/bash

#
# Pipeline that submits jobs to a LSF cluster
# @author: sdentro
# @date: April 17, 2013
#
#

FQ1=`basename $2`
FQ2=`basename $3`
FQ1_path=`dirname $2`
FQ2_path=`dirname $3`

#FQ1=CRA00Rb_1.subs.fastq.gz
#FQ2=CRA00Rb_2.subs.fastq.gz
#FQ1_path=/lustre/scratch102/user/sd11/realignment
#FQ2_path=/lustre/scratch102/user/sd11/realignment


source config


SAMPLE=`echo $FQ1 | cut -d . -f 1 | sed 's/\_1\|\_2//'`
TEMPNAME=temp
LOGNAME=log

splitfq () {
	fq=$1
	outdir=$2
	prefix=$3
	lines=$4
	zcat $fq | split -l $lines -a 10 -d - $outdir/$prefix.
	## TODO gzip
}

mkdir $BASE/$SAMPLE
mkdir $BASE/$SAMPLE/$TEMPNAME
mkdir $BASE/$SAMPLE/$LOGNAME

TEMP=$BASE/$SAMPLE/$TEMPNAME
LOG=$BASE/$SAMPLE/$LOGNAME

#############################
# SWITCHES
############################
ARG="-$1"
if   [ $ARG == '-aln' ] ; then
	##########
        echo "aln"
	##########
	ln -s ${FQ1_path}/$FQ1 $TEMP/$FQ1
	ln -s ${FQ2_path}/$FQ2 $TEMP/$FQ2

	splitfq $TEMP/$FQ1 $TEMP ${SAMPLE}_1 $SPLITSIZE
	splitfq $TEMP/$FQ2 $TEMP ${SAMPLE}_2 $SPLITSIZE

	splits=`ls $TEMP | grep _1 | grep -v $FQ1`


	jobstowaitfor=""
	for item in $splits; do
		message=`bsub -J ${SAMPLE}_aln_${item} -q $QUEUE -M 2000000 -R 'span[hosts=1] select[mem>2000] rusage[mem=2000]' -o $LOG/${SAMPLE}_gz_${item}.%J.out -e $LOG/${SAMPLE}_gz_${item}.%J.err gzip $TEMP/$item`
		jobid=`echo $message | cut -d "<" -f 2 | cut -d ">" -f 1`

		message=`bsub -w done($jobid) -J ${SAMPLE}_aln_${item}.gz -q $QUEUE -M 8000000 -R 'span[hosts=1] select[mem>8000] rusage[mem=8000]' -o $LOG/${SAMPLE}_aln_${item}.gz.%J.out -e $LOG/${SAMPLE}_aln_${item}..gz%J.err $SCRIPTS/bwa_align.sh $item.gz $SAMPLE $TEMP $REF`
                jobid=`echo $message | cut -d "<" -f 2 | cut -d ">" -f 1`
                jobstowaitfor=$jobstowaitfor" && done($jobid)"
		
		## TODO: remove fastq files
	done

	## Remove first and-and
	jobstowaitfor=`echo $jobstowaitfor | sed 's/\&\&//'`

	bsub -w "$jobstowaitfor" -J ${SAMPLE} -q $QUEUE -M 500 -R 'span[hosts=1] select[mem>50] rusage[mem=50]' -o $LOG/${SAMPLE}_complete.%J.out -e $LOG/${SAMPLE}_complete.%J.err touch $TEMP/DONE_ALN


elif [ $ARG == '-fix' ] ; then
	###########
        echo "FIX"
	###########
	
	for item in `ls $TEMP | grep sam.bam`; do
		message=`bsub -J ${SAMPLE}_sam_${item} -q $QUEUE -M 6000000 -R 'span[hosts=1] select[mem>6000] rusage[mem=6000]' -o $LOG/${SAMPLE}_sam_${item}.%J.out -e $LOG/${SAMPLE}_sam_${item}.%J.err $SCRIPTS/bwa_sam.sh $SAMPLE $item $TEMP $REF`
		jobid=`echo $message | cut -d "<" -f 2 | cut -d ">" -f 1`
                jobstowaitfor=$jobstowaitfor" && done($jobid)"
	done

        ## Remove first and-and
        jobstowaitfor=`echo $jobstowaitfor | sed 's/\&\&//'`

	message=`bsub -w "$jobstowaitfor" -J ${SAMPLE}_merge -q $QUEUE -M 4000000 -R 'span[hosts=1] select[mem>4000] rusage[mem=4000]' -o $LOG/${SAMPLE}_merge.%J.out -e $LOG/${SAMPLE}_merge.%J.err $SCRIPTS/bam_merge.sh $SAMPLE $TEMP`
	jobid=`echo $message | cut -d "<" -f 2 | cut -d ">" -f 1`

	message=`bsub -w "done($jobid)" -J ${SAMPLE}_index -q $QUEUE -M 1000000 -R 'span[hosts=1] select[mem>1000] rusage[mem=1000]' -o $LOG/${SAMPLE}_index.%J.out -e $LOG/${SAMPLE}_index.%J.err samtools index $TEMP/$SAMPLE".bam"`
	jobid=`echo $message | cut -d "<" -f 2 | cut -d ">" -f 1`
	
	bsub -w "done($jobid)" -J ${SAMPLE} -q normal -M 500 -R 'span[hosts=1] select[mem>50] rusage[mem=50]' -o $LOG/${SAMPLE}_complete.%J.out -e $LOG/${SAMPLE}_complete.%J.err touch $TEMP/DONE_FIX


else
        echo "Stage argument '$1' missing/unknown"
        exit 1
fi
