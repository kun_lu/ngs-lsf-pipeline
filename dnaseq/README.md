
Readme
======
Alignment pipeline that submits jobs to an LSF job scheduling system.

@author: sdentro
@date: April 17, 2013


How to run
==========
pipeline.sh [command] fastq_1 fastq_2
