#!/bin/bash

SAMPLE=$1
TEMP=$2


MERGEBAM="/software/vertres/bin-external/picard-tools-1.64/MergeSamFiles.jar"


bams=`ls $TEMP | grep fixed.bam`
command="java -Xmx4g -jar $MERGEBAM"
for item in $bams; do
	command="$command I=$TEMP/$item"
done
command="$command O=$TEMP/$SAMPLE.bam SO=coordinate USE_THREADING=true VALIDATION_STRINGENCY=LENIENT"
$command
