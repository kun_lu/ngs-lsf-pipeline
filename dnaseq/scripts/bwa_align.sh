#!/bin/bash

item=$1
SAMPLE=$2
TEMP=$3
REF=$4

item_mate=`echo $item | sed 's/\_1/_2/'`
suffix=`echo $item | rev | cut -d . -f 1 | rev`
bwa aln -q 15 -f $TEMP/$item".sai" $REF $TEMP/$item
echo "bwa aln -q 15 -f $TEMP/$item".sai" $REF $TEMP/$item"

bwa aln -q 15 -f $TEMP/${item_mate}".sai" $REF $TEMP/${item_mate}
echo "bwa aln -q 15 -f $TEMP/${item_mate}".sai" $REF $TEMP/${item_mate}"

rg="'@RG\tID:${SAMPLE}\tSM:${SAMPLE}\tPL:Illumina\tPU:WGS'"
rg='@RG\tID:sample\tSM:sample\tPL:Illumina\tPU:WGS'
echo "bwa sampe -r $rg -f $TEMP/$SAMPLE.$suffix.sam $REF $TEMP/$item".sai" $TEMP/${item_mate}".sai" $TEMP/$item $TEMP/${item_mate}"
bwa sampe -r $rg -f $TEMP/$SAMPLE.$suffix.sam $REF $TEMP/$item".sai" $TEMP/${item_mate}".sai" $TEMP/$item $TEMP/${item_mate}
samtools view -bS $TEMP/$SAMPLE.$suffix.sam > $TEMP/$SAMPLE.$suffix.sam.bam
