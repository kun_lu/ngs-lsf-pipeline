#!/bin/bash

SAMPLE=$1
BAM=$2
TEMP=$3
REF=$4

samtools view -bu $TEMP/$BAM | samtools sort -n -o - $BAM.samtools_nsort_tmp | samtools fixmate /dev/stdin /dev/stdout | samtools sort -o - $BAM.samtools_csort_tmp |  samtools fillmd -b - $REF > $TEMP/$BAM.fixed.bam
